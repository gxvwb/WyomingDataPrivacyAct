An Act relating to data protection and privacy in the State of Wyoming

Section 1. Short title

This act shall be known and may be cited as the "Wyoming Data Privacy Act."

Section 2. Definitions

(a) "Personal data" means any information relating to an identified or identifiable natural person, as defined in the GDPR.

(b) "Data controller" means a natural or legal person, public authority, agency or other body which, alone or jointly with others, determines the purposes and means of the processing of personal data, as defined in the GDPR.

(c) "Data processor" means a natural or legal person, public authority, agency or other body which processes personal data on behalf of the data controller, as defined in the GDPR.

Section 3. Applicability

This act applies to any data controller or data processor that processes the personal data of Wyoming residents, regardless of whether the entity is located in Wyoming.

Section 4. Rights of Wyoming Residents

(a) Right to be Informed: Data controllers shall provide Wyoming residents with clear and concise information about the processing of their personal data in a transparent, easily accessible, and understandable manner.

(b) Right to Access: Wyoming residents shall have the right to obtain from a data controller, without undue delay, confirmation as to whether or not personal data concerning them are being processed, and where that is the case, access to the personal data and related information.

(c) Right to Correction: Wyoming residents shall have the right to have inaccurate personal data corrected without undue delay.

(d) Right to Erasure: Wyoming residents shall have the right to have personal data concerning them erased without undue delay.

(e) Right to Restriction of Processing: Wyoming residents shall have the right to restrict the processing of their personal data in certain circumstances.

(f) Right to Data Portability: Wyoming residents shall have the right to receive the personal data concerning them, which they have provided to a data controller, in a structured, commonly used, and machine-readable format.

(g) Right to Object: Wyoming residents shall have the right to object, on grounds relating to their particular situation, to the processing of their personal data, unless the data controller demonstrates compelling legitimate grounds for the processing which override the interests, rights, and freedoms of the Wyoming resident or for the establishment, exercise or defense of legal claims.

Section 5. Data Breach Notification

(a) Data controllers shall notify Wyoming residents without undue delay in the event of a data breach that is likely to result in a high risk to their rights and freedoms.

(b) Data controllers shall notify the Wyoming Attorney General's office of any data breaches that meet certain criteria, as defined in this act.

Section 6. Enforcement

The Wyoming Attorney General shall be responsible for enforcement of this act. Any person or entity found to be in violation of this act shall be subject to penalties as determined by the Wyoming Attorney General.

Section 7. Effective Date

This act shall take effect on January 1, 2024.